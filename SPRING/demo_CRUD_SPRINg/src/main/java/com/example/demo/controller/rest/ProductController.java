package com.example.demo.controller.rest;

import com.example.demo.entity.Product;
import com.example.demo.repository.ProductRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.sql.SQLException;
import java.util.List;

@RestController
@RequestMapping(value = "/products")
public class ProductController {

    @Autowired
    private ProductRepo productRepo;

    // Create Operations
    @RequestMapping(method = RequestMethod.POST)
    public Product createProduct(@RequestBody Product product) {
        return (Product) productRepo.save(product);
    }

    // Read
    @RequestMapping(method = RequestMethod.GET)
    public List<Product> getProducts() {
        return productRepo.findAll();
    }

    // Update operation
    public boolean updateProduct(Product product) {
        // Xem tren example bitbucket :d
        return true;
    }

    // Delete
    @RequestMapping(method = RequestMethod.DELETE)
    public String delete(int id) {
        productRepo.deleteById(id);
        return "Delete product with id " + id + " completed !";
    }
}
