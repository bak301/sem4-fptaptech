package com.example.demo.controller;

import com.example.demo.entity.User;
import com.example.demo.repository.UserRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.persistence.NoResultException;

@Controller
@RequestMapping(value = "/index")
public class HomeController {

    @Autowired
    private UserRepo userRepo;

    @RequestMapping(method = RequestMethod.GET)
    public String index() {
        return "index.html";
    }

    @RequestMapping(value = "/product", method = RequestMethod.GET)
    public String product() {
        return "product.html";
    }

    @RequestMapping(method = RequestMethod.POST)
    @ResponseBody
    public String login(@RequestParam("username") String user, @RequestParam("password") String pass) {
        User result = userRepo.findUserByUsernameAndPassword(user, pass);
        return result != null ? "Login success" : "Login failed !";
    }
}
