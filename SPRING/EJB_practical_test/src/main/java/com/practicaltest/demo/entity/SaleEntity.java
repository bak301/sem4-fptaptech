package com.practicaltest.demo.entity;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

@Entity
@Table(name = "sale", schema = "public", catalog = "ejb_test")
public class SaleEntity {
    private int slno;
    private Integer salesmanid;
    private Integer prodid;
    private String salesmanname;
    private Date dos;

    @Id
    @Column(name = "slno")
    public int getSlno() {
        return slno;
    }

    public void setSlno(int slno) {
        this.slno = slno;
    }

    @Basic
    @Column(name = "salesmanid")
    public Integer getSalesmanid() {
        return salesmanid;
    }

    public void setSalesmanid(Integer salesmanid) {
        this.salesmanid = salesmanid;
    }

    @Basic
    @Column(name = "prodid")
    public Integer getProdid() {
        return prodid;
    }

    public void setProdid(Integer prodid) {
        this.prodid = prodid;
    }

    @Basic
    @Column(name = "salesmanname")
    public String getSalesmanname() {
        return salesmanname;
    }

    public void setSalesmanname(String salesmanname) {
        this.salesmanname = salesmanname;
    }

    @Basic
    @Column(name = "dos")
    public Date getDos() {
        return dos;
    }

    public void setDos(Date dos) {
        this.dos = dos;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SaleEntity that = (SaleEntity) o;
        return slno == that.slno &&
                Objects.equals(salesmanid, that.salesmanid) &&
                Objects.equals(prodid, that.prodid) &&
                Objects.equals(salesmanname, that.salesmanname) &&
                Objects.equals(dos, that.dos);
    }

    @Override
    public int hashCode() {
        return Objects.hash(slno, salesmanid, prodid, salesmanname, dos);
    }
}
