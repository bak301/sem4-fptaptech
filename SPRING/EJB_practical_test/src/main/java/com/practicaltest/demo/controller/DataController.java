package com.practicaltest.demo.controller;

import com.practicaltest.demo.entity.ProductEntity;
import com.practicaltest.demo.entity.SaleEntity;
import com.practicaltest.demo.repo.ProductRepo;
import com.practicaltest.demo.repo.SaleRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class DataController {

    private final ProductRepo productRepo;

    private final SaleRepo saleRepo;

    @Autowired
    public DataController(ProductRepo productRepo, SaleRepo saleRepo) {
        this.productRepo = productRepo;
        this.saleRepo = saleRepo;
    }

    @RequestMapping(value = "/products")
    public List<ProductEntity> product() {
        return productRepo.findAll();
    }

    @RequestMapping(value = "/sales")
    public List<SaleEntity> sale() {
        return saleRepo.findAll();
    }
}
