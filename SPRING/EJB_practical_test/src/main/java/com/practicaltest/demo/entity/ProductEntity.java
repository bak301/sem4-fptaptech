package com.practicaltest.demo.entity;

import javax.persistence.*;
import java.sql.Date;
import java.util.Objects;

@Entity
@Table(name = "product", schema = "public", catalog = "ejb_test")
public class ProductEntity {
    private int prodid;
    private String prodname;
    private String description;
    private Date dof;
    private Double price;

    @Id
    @Column(name = "prodid")
    public int getProdid() {
        return prodid;
    }

    public void setProdid(int prodid) {
        this.prodid = prodid;
    }

    @Basic
    @Column(name = "prodname")
    public String getProdname() {
        return prodname;
    }

    public void setProdname(String prodname) {
        this.prodname = prodname;
    }

    @Basic
    @Column(name = "description")
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "dof")
    public Date getDof() {
        return dof;
    }

    public void setDof(Date dof) {
        this.dof = dof;
    }

    @Basic
    @Column(name = "price")
    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProductEntity that = (ProductEntity) o;
        return prodid == that.prodid &&
                Objects.equals(prodname, that.prodname) &&
                Objects.equals(description, that.description) &&
                Objects.equals(dof, that.dof) &&
                Objects.equals(price, that.price);
    }

    @Override
    public int hashCode() {
        return Objects.hash(prodid, prodname, description, dof, price);
    }
}
