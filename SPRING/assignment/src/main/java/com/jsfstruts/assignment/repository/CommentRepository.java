package com.jsfstruts.assignment.repository;

import com.jsfstruts.assignment.entity.Comment;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.Repository;

import java.util.List;

public interface CommentRepository extends JpaRepository<Comment, Integer> {

    @Query("select c from Comment c where c.guide_id = ?1")
    List<Comment> findAllByGuide_id(int guide_id);
}
