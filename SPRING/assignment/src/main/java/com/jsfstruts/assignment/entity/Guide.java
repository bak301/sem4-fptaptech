package com.jsfstruts.assignment.entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@NamedStoredProcedureQuery(
        name = "upvote",
        procedureName = "upvote",
        parameters = {
                @StoredProcedureParameter(
                        name = "guideId",
                        type = Integer.class,
                        mode = ParameterMode.IN
                )
        }
)
public class Guide {
    private int id;
    private int user_id;
    private String placeName;
    private String description;
    private String imageUrl;
    private Integer rating;

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "user_id", nullable = false)
    public int getUser_id() {
        return user_id;
    }

    public void setUser_id(int user_id) {
        this.user_id = user_id;
    }

    @Basic
    @Column(name = "place_name", nullable = true, length = 50)
    public String getPlaceName() {
        return placeName;
    }

    public void setPlaceName(String placeName) {
        this.placeName = placeName;
    }

    @Basic
    @Column(name = "description", nullable = true, length = 2048)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Basic
    @Column(name = "image_url", nullable = true, length = 50)
    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    @Basic
    @Column(name = "rating", nullable = true)
    public Integer getRating() {
        return rating;
    }

    public void setRating(Integer rating) {
        this.rating = rating;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Guide guide = (Guide) o;
        return id == guide.id &&
                Objects.equals(placeName, guide.placeName) &&
                Objects.equals(description, guide.description) &&
                Objects.equals(imageUrl, guide.imageUrl) &&
                Objects.equals(rating, guide.rating);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }
}
