package com.jsfstruts.assignment.repository;

import com.jsfstruts.assignment.entity.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;

public interface  UserRepository extends JpaRepository<User, Integer> {

    @Query("select u from User u where u.username = ?1 and u.password = ?2")
    User findByUsernameAndPassword(String username, String password);

    @Query("select case when count(u) > 0 then true else false end from User u where u.username = ?1 and u.password = ?2")
    boolean isExist(String username, String password);

    @Query("select u from User u where u.username = ?1")
    User findByUsername(String username);
}
