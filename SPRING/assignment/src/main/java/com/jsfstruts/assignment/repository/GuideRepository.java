package com.jsfstruts.assignment.repository;

import com.jsfstruts.assignment.entity.Guide;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.jpa.repository.query.Procedure;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import javax.persistence.NamedStoredProcedureQueries;
import javax.persistence.StoredProcedureParameter;
import java.util.List;

public interface GuideRepository extends JpaRepository<Guide, Integer> {

    @Query("select g from Guide g where place_name like concat('%', :username, '%') ")
    List<Guide> findGuidesByPlaceName(@Param("username")String name);

    @Procedure("upvote")
    void upvote(@Param("guideId") int index);
}
