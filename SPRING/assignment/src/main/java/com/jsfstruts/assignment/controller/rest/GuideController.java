package com.jsfstruts.assignment.controller.rest;

import com.jsfstruts.assignment.entity.Guide;
import com.jsfstruts.assignment.repository.GuideRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/guides")
public class GuideController {

    private GuideRepository guideRepository;

    @Autowired
    public GuideController(GuideRepository guideRepository) {
        this.guideRepository = guideRepository;
    }

    // GET
    @RequestMapping(method = RequestMethod.GET)
    public List<Guide> getGuidesByName(@RequestParam(value = "name", defaultValue = "") String name) {
        return "".equals(name) ? guideRepository.findAll() : guideRepository.findGuidesByPlaceName(name);
    }

    @RequestMapping(value = "/{index}", method = RequestMethod.GET)
    public Guide getGuide(@PathVariable int index) {
        return guideRepository.getOne(index);
    }

    // POST
    @RequestMapping(method = RequestMethod.POST)
    public Guide createGuide(@RequestBody Guide guide) {
        return guideRepository.save(guide);
    }

    //PUT
    @RequestMapping(value = "/{index}", method = RequestMethod.PUT)
    public Guide updateGuide(@PathVariable int index, @RequestBody Guide guide) {

        Guide originalGuide = guideRepository.getOne(index);

        originalGuide.setPlaceName(guide.getPlaceName());
        originalGuide.setDescription(guide.getDescription());
        originalGuide.setImageUrl(guide.getImageUrl());

        return guideRepository.save(originalGuide);
    }

    //DELETE
    @RequestMapping(value = "/{index}", method = RequestMethod.DELETE)
    public void deleteGuide(@PathVariable int index) {
        guideRepository.deleteById(index);
    }

    //PATCH for upvote
    @RequestMapping(value = "/{index}", method = RequestMethod.PATCH)
    public void upvote(@PathVariable int index) {
        guideRepository.upvote(index);
    }
}
