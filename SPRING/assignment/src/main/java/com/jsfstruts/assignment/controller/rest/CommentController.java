package com.jsfstruts.assignment.controller.rest;

import com.jsfstruts.assignment.entity.Comment;
import com.jsfstruts.assignment.repository.CommentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class CommentController {
    private CommentRepository commentRepository;

    @Autowired
    public CommentController(CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
    }

    //GET
    @RequestMapping(value = "/guides/{guideIndex}/comments", method = RequestMethod.GET)
    public List<Comment> getComments(@PathVariable int guideIndex) {
        return commentRepository.findAllByGuide_id(guideIndex);
    }

    //POST
    @RequestMapping(value = "/guides/*/comments", method = RequestMethod.POST)
    public Comment createComment(@RequestBody Comment comment) {
        return commentRepository.save(comment);
    }
}
