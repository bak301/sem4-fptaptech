package com.example.ptest.controller;

import com.example.ptest.entity.Product;
import com.example.ptest.repository.ProductRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;

@RestController
@RequestMapping(value = "/products")
public class ProductController {

    private final ProductRepo repo;

    @Autowired
    public ProductController(ProductRepo repo) {
        this.repo = repo;
    }

    @RequestMapping(method = RequestMethod.GET)
    public List<Product> getAllProducts() {
        return repo.findAll();
    }

    @RequestMapping(method = RequestMethod.POST)
    public Product addProduct(@RequestBody Product p) {
        return repo.save(p);
    }

    @RequestMapping(value = "/{productId}", method = RequestMethod.PATCH)
    public String sellProduct(@PathVariable int productId, @RequestParam(value = "quantity") int quantity) {
        try {
            repo.decreaseQuantity(productId, quantity);
            return "OK";
        } catch (Exception ex) {
            return "Error occured : " + ex.getMessage();
        }
    }
}
