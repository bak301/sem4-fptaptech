package com.example.ptest.repository;

import com.example.ptest.entity.Product;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;

import javax.transaction.Transactional;

public interface ProductRepo extends JpaRepository<Product, Integer> {

    @Transactional
    @Modifying
    @Query("update Product p set p.quantity = p.quantity - ?2 where p.id = ?1")
    public void decreaseQuantity(int productId, int quantity);
}
