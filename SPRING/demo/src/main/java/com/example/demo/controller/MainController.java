package com.example.demo.controller;

import com.example.demo.DA.PhoneRepository;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/")
public class MainController {

    private PhoneRepository repository;

    public MainController(PhoneRepository repository) {
        this.repository = repository;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String index(Model model) {
        model.addAttribute("phones", repository.findAll());
        return "index";
    }

    @RequestMapping(value = "/phones/{name}", method = RequestMethod.GET)
    public String getPhones(Model model,@PathVariable String name) {
        model.addAttribute("phones", repository.findByNameContains(name));
        return "index";
    }
}
