package com.example.demo.DA;

import com.example.demo.entity.Phone;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PhoneRepository extends JpaRepository<Phone, Integer> {
    @Query("select p from Phone p where p.name like concat('%',:name,'%')")
    List<Phone> findByNameContains(@Param("name") String name);
}
