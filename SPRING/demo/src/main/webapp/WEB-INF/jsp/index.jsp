<%@ page contentType="text/html; charset = UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>Hello World</title>
</head>

<body>
<h2>Phone List</h2>
    <c:forEach items="${phones}" var="phone">
        <div>${phone.name}</div>
    </c:forEach>
</body>
</html>