package com.example.ijx.controller;

import com.example.ijx.entity.Customers;
import com.example.ijx.repository.CustomerRepo;
import com.example.ijx.xml.CustomerXMLHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class HomeController {

    private final CustomerRepo repo;
    private CustomerXMLHandler xmlHandler;
    private static String XML_FILE_NAME = "Customers.xml";
    private static String SCHEMA_FILE_NAME = "Customers.xsd";

    @Autowired
    public HomeController(CustomerRepo repo) {
        this.xmlHandler = new CustomerXMLHandler(XML_FILE_NAME);
        this.repo = repo;
    }

    @RequestMapping(value = "/export")
    public String export() {
        List<Customers> customersList= repo.findAll(PageRequest.of(0, 50)).getContent();
        return xmlHandler.export(customersList) ? "Export completed" : "Export error";
    }

    @RequestMapping(value = "/validate")
    public String validate() {
        return xmlHandler.validate(SCHEMA_FILE_NAME, XML_FILE_NAME) ? "File validated !" : "File validation failed !";
    }

    @RequestMapping(value = "/show")
    public List<Customers> show() {
        return xmlHandler.getCustomerList();
    }
}
