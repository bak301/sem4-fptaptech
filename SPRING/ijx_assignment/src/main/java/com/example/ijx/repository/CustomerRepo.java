package com.example.ijx.repository;

import com.example.ijx.entity.Customers;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;

public interface CustomerRepo extends PagingAndSortingRepository<Customers, String> {

    @Query("select c.contactName, c.contactTitle, c.companyName, c.address, c.city, c.postalCode, c.country, c.phone, c.fax from Customers c")
    Page<Customers> findAllWithCustomColumns(Pageable pageable);
}

