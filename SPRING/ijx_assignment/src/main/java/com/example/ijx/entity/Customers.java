package com.example.ijx.entity;

import javax.persistence.*;
import java.util.Arrays;
import java.util.Objects;

@Entity
@Table(name = "customers")
public class Customers {
    private String customerId;
    private String companyName;
    private String contactName;
    private String contactTitle;
    private String address;
    private String city;
    private String postalCode;
    private String country;
    private String phone;
    private String fax;

    @Id
    @Column(name = "CustomerID", nullable = false, length = 5)
    public String getCustomerId() {
        return customerId;
    }

    public void setCustomerId(String customerId) {
        this.customerId = customerId;
    }

    @Basic
    @Column(name = "CompanyName", nullable = false, length = 40)
    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    @Basic
    @Column(name = "ContactName", nullable = true, length = 30)
    public String getContactName() {
        return contactName;
    }

    public void setContactName(String contactName) {
        this.contactName = contactName;
    }

    @Basic
    @Column(name = "ContactTitle", nullable = true, length = 30)
    public String getContactTitle() {
        return contactTitle;
    }

    public void setContactTitle(String contactTitle) {
        this.contactTitle = contactTitle;
    }

    @Basic
    @Column(name = "Address", nullable = true, length = 60)
    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Basic
    @Column(name = "City", nullable = true, length = 15)
    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    @Basic
    @Column(name = "PostalCode", nullable = true, length = 10)
    public String getPostalCode() {
        return postalCode;
    }

    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    @Basic
    @Column(name = "Country", nullable = true, length = 15)
    public String getCountry() {
        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    @Basic
    @Column(name = "Phone", nullable = true, length = 24)
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Basic
    @Column(name = "Fax", nullable = true, length = 24)
    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Customers customers = (Customers) o;
        return Objects.equals(customerId, customers.customerId) &&
                Objects.equals(companyName, customers.companyName) &&
                Objects.equals(contactName, customers.contactName) &&
                Objects.equals(contactTitle, customers.contactTitle) &&
                Objects.equals(address, customers.address) &&
                Objects.equals(city, customers.city) &&
                Objects.equals(postalCode, customers.postalCode) &&
                Objects.equals(country, customers.country) &&
                Objects.equals(phone, customers.phone) &&
                Objects.equals(fax, customers.fax);
    }

    @Override
    public int hashCode() {
        return Objects.hash(customerId, companyName, contactName, contactTitle, address, city, postalCode, country, phone, fax);
    }
}
