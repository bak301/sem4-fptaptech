package com.example.ijx.model;

import com.example.ijx.entity.Customers;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.util.List;

@XmlRootElement(name = "customers")
@XmlAccessorType(value = XmlAccessType.FIELD)
public class CustomerXMLWrapper {

    @XmlElement(name = "customer")
    private List<Customers> customersList;

    public CustomerXMLWrapper() {
    }

    public CustomerXMLWrapper(List<Customers> customersList) {
        this.customersList = customersList;
    }

    public List<Customers> getCustomersList() {
        return customersList;
    }

    public void setCustomersList(List<Customers> customersList) {
        this.customersList = customersList;
    }
}
