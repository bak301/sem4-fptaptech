package com.example.ijx;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IjxApplication {

    public static void main(String[] args) {
        SpringApplication.run(IjxApplication.class, args);
    }
}
