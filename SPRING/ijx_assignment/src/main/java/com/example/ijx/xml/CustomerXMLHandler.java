package com.example.ijx.xml;

import com.example.ijx.entity.Customers;
import com.example.ijx.model.CustomerXMLWrapper;
import org.springframework.oxm.XmlMappingException;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.bind.JAXBContext;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;
import java.util.List;

public class CustomerXMLHandler {
    private File file;
    private Jaxb2Marshaller marshaller;

    public CustomerXMLHandler(String fileName) {
        this.file = new File(fileName);
        this.marshaller = new Jaxb2Marshaller();
        marshaller.setClassesToBeBound(CustomerXMLWrapper.class);
    }

    public boolean validate(String schemaFile, String xmlFile) {
        SchemaFactory schemaFactory = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
        try {
            Schema schema = schemaFactory.newSchema(new File(schemaFile));

            Validator validator = schema.newValidator();
            validator.validate(new StreamSource(new File(xmlFile)));
            return true;
        } catch (SAXException | IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public List<Customers> getCustomerList() {
        return (
                (CustomerXMLWrapper) marshaller.unmarshal(new StreamSource(file))
        ).getCustomersList();
    }

    public boolean export(List<Customers> customersList) {
        try {
            StreamResult result = new StreamResult(file);
            marshaller.marshal(new CustomerXMLWrapper(customersList), result);
            return true;
        } catch (XmlMappingException ex) {
            ex.printStackTrace();
        }
        return false;
    }
}
