package com.example.bak.demorecyclerview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.Toast;

import com.example.bak.demorecyclerview.Model.Product;
import com.example.bak.demorecyclerview.Model.ProductAdapter;

import java.util.ArrayList;
import java.util.List;

public class Home extends AppCompatActivity implements ProductAdapter.IOnclickItem{

    List<Product> products = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        initMockData();

        ProductAdapter adapter = new ProductAdapter(this, products, this);
        GridLayoutManager layoutManager = new GridLayoutManager(this, 1);

        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.products);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    private void initMockData() {
        products.add(new Product("InWin H Frame 2.0", "In Win Golden case with tempered glass", 345, R.drawable.inwin_h_frame_2));
        products.add(new Product("Lian-li", "Lian Li RGB case", 189, R.drawable.lian_li_case));
        products.add(new Product("Thermaltake Transparent", "A beautiful Thermaltake case", 224, R.drawable.thermaltake_transparent));
        products.add(new Product("Phanteks Etho Evolv TP", "Signature Phanteks Case", 289, R.drawable.phanteks_ethoo_evolv_tp_3));
    }

    @Override
    public void onClickItem(int position) {
        Product product = products.get(position);
        Toast.makeText(this, product.getTitle(), Toast.LENGTH_LONG).show();
    }
}
