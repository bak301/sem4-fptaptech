package com.example.bak.demorecyclerview.Model;

public class Product {
    private String title;
    private String description;
    private int price;
    private int cover;

    public Product(String title, String description, int price, int cover) {
        this.title = title;
        this.description = description;
        this.price = price;
        this.cover = cover;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getCover() {
        return cover;
    }

    public void setCover(int cover) {
        this.cover = cover;
    }
}
