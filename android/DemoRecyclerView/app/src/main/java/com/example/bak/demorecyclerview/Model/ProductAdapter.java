package com.example.bak.demorecyclerview.Model;

import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.bak.demorecyclerview.R;

import java.util.List;

public class ProductAdapter extends RecyclerView.Adapter {

    private Activity activity;
    private List<Product> products;
    private IOnclickItem onclickItem;

    public ProductAdapter(Activity activity, List<Product> products, IOnclickItem onclickItem) {
        this.activity = activity;
        this.products = products;
        this.onclickItem = onclickItem;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater inflater = activity.getLayoutInflater();
        View view = inflater.inflate(R.layout.layout_product, viewGroup, false);
        return new ProductHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        ProductHolder holder = (ProductHolder) viewHolder;
        Product product = products.get(i);

        holder.cover.setImageResource(product.getCover());

//        holder.title.setText(product.getTitle());
//        holder.description.setText(product.getDescription());
//        holder.price.setText(String.valueOf(product.getPrice()));

        holder.itemView.setOnClickListener((view) -> onclickItem.onClickItem(i));
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    public static class ProductHolder extends RecyclerView.ViewHolder {

        private ImageView cover;
        private TextView title;
        private TextView description;
        private TextView price;

        public ProductHolder(@NonNull View itemView) {
            super(itemView);
            cover = (ImageView) itemView.findViewById(R.id.cover);
            title = (TextView) itemView.findViewById(R.id.title);
            description = (TextView) itemView.findViewById(R.id.description);
            price = (TextView) itemView.findViewById(R.id.price);
        }
    }

    public interface IOnclickItem {
        void onClickItem(int position);
    }
}
