package com.example.bak.weather.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.bak.weather.R;
import com.example.bak.weather.model.Weather;

import org.w3c.dom.Text;

import java.util.List;

public class HourAdapter extends RecyclerView.Adapter {

    List<Weather> weathers;
    private Activity activity;

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int type) {
        LayoutInflater inflater = activity.getLayoutInflater();
//        View itemView = inflater.inflate(R.layout)
        return null;
    }

    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
        HourHolder holder = (HourHolder) viewHolder;
        Weather weather = weathers.get(i);

        holder.time.setText(weather.getDateTime());
        holder.tem.setText(weather.getTemperature().getMinTemp() + " - " + weather.getTemperature().getMaxTemp());

        int icon = weather.getWeatherIcon();

        String url = "https://developer.accuweather.com/sites/default/files/"
                + ( icon < 10
                ? "0" + icon
                : icon) +"-s.pgn";
    }

    @Override
    public int getItemCount() {
        return 0;
    }

    public static class HourHolder extends RecyclerView.ViewHolder {
        private TextView time;
        private ImageView icon;
        private TextView tem;

        public HourHolder(@NonNull View itemView) {
            super(itemView);
            time = itemView.findViewById(R.id.time);
            icon = itemView.findViewById(R.id.icon);
            tem = itemView.findViewById(R.id.tem);
        }
    }
}
