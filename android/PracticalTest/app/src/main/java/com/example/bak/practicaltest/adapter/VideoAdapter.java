package com.example.bak.practicaltest.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.bak.practicaltest.R;
import com.example.bak.practicaltest.model.Video;

import java.util.List;

public class VideoAdapter extends BaseAdapter {

    private List<Video> videos;
    private Activity activity;

    public VideoAdapter(List<Video> videos, Activity activity) {
        this.videos = videos;
        this.activity = activity;
    }

    @Override
    public int getCount() {
        return videos.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        String youtubeURL = "https://www.youtube.com/watch?v=";

        if (convertView == null) {
            LayoutInflater inflater = activity.getLayoutInflater();
            convertView = inflater.inflate(R.layout.layout_video, parent, false);
            ViewHolder holder = new ViewHolder();

            holder.link = convertView.findViewById(R.id.youtubeLink);
            holder.name = convertView.findViewById(R.id.name);
            holder.description = convertView.findViewById(R.id.description);
            convertView.setTag(holder);
        }

        ViewHolder holder = (ViewHolder) convertView.getTag();
        Video video = videos.get(position);
        String link = youtubeURL + video.getVideoId();

        holder.link.setText(link);
        holder.name.setText(video.getName() == null ? "" : video.getName());
        holder.description.setText(video.getDescription() == null ? "" : video.getDescription());

        return convertView;
    }

    public static class ViewHolder {
        TextView link;
        TextView name;
        TextView description;
    }
}
