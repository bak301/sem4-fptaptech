package com.example.bak.practicaltest.API;

import com.example.bak.practicaltest.model.Video;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface ApiManager {
    String BASE_URL = "http://youtube-video-api-1608.appspot.com";

    @GET("/youtube/api")
    Call<List<Video>> getFeed();
}
