package com.example.bak.practicaltest;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import com.example.bak.practicaltest.API.ApiManager;
import com.example.bak.practicaltest.adapter.VideoAdapter;
import com.example.bak.practicaltest.model.Video;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Home extends AppCompatActivity {

    private ListView listVideos;
    private List<Video> videos;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        initData();
    }

    private void initData() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiManager.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        ApiManager service = retrofit.create(ApiManager.class);

        service.getFeed().enqueue(new Callback<List<Video>>() {
            @Override
            public void onResponse(Call<List<Video>> call, Response<List<Video>> response) {
                if (response.body() == null) return;
                videos = response.body();
                for (Video v : videos) {
                    Log.d("TEST", "VIDEO Id: " + v.getVideoId());
                }
                VideoAdapter adapter = new VideoAdapter(videos, Home.this);
                listVideos = findViewById(R.id.videos);
                listVideos.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<List<Video>> call, Throwable t) {
                Log.d("ERROR", "ERROR : CANNOT GET DATA :" + t.getMessage());
            }
        });
    }
}
