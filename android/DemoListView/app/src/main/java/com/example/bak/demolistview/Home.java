package com.example.bak.demolistview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

public class Home extends AppCompatActivity implements OnChildItemClick {

    private ImageView mainAvatar;
    private TextView mainName;
    private ContactAdapter adapter;
    private ListView contacts;
    private List<ContactModel> modelList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        initData();

        mainAvatar = findViewById(R.id.mainAvatar);
        mainName = findViewById(R.id.mainName);
        contacts = findViewById(R.id.listContact);
        adapter = new ContactAdapter(modelList, this);
        adapter.registerChildItemClick(this);
        contacts.setAdapter(adapter);


        contacts.setOnItemClickListener((parent, view, pos, id) -> {
            ContactModel contactModel = modelList.get(pos);
            Toast.makeText(Home.this, contactModel.getName(), Toast.LENGTH_LONG).show();
        });
    }

    private void initData() {
        modelList.add(new ContactModel("Vu Tung", "0917273612", R.mipmap.ic_launcher));
    }


    @Override
    public void onItemChildClick(int position) {
        ContactModel contact = modelList.get(position);
        mainName.setText(contact.getName());
        mainAvatar.setImageResource(contact.getImage());
    }
}
