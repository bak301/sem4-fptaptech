package com.example.bak.demolistview;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.support.v4.app.ActivityCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

public class ContactAdapter extends BaseAdapter {

    private List<ContactModel> modelList;
    private Activity activity;
    private OnChildItemClick onChildItemClick;

    public ContactAdapter(List<ContactModel> modelList, Activity activity) {
        this.modelList = modelList;
        this.activity = activity;
    }

    public void registerChildItemClick(OnChildItemClick handler) {
        this.onChildItemClick = handler;
    }

    public void unregisterChildItemClick() {
        this.onChildItemClick = null;
    }

    @Override
    public int getCount() {
        return modelList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = activity.getLayoutInflater();
            convertView = inflater.inflate(R.layout.layout_contact, parent, false);

            ViewHolder holder = new ViewHolder();
            holder.name = convertView.findViewById(R.id.name);
            holder.phone = convertView.findViewById(R.id.phone);
            holder.avatar = convertView.findViewById(R.id.avatar);
            holder.call = convertView.findViewById(R.id.call);
            holder.edit = convertView.findViewById(R.id.edit);
            convertView.setTag(holder);
        }

        ViewHolder holder = (ViewHolder) convertView.getTag();
        ContactModel model = modelList.get(position);

        holder.name.setText(model.getName());
        holder.phone.setText(model.getPhone());
        holder.avatar.setImageResource(model.getImage());
        holder.call.setOnClickListener((view) -> {
            onCall(position);
        });
        holder.edit.setOnClickListener((view) -> {
            onChildItemClick.onItemChildClick(position);
        });

        return convertView;
    }

    private void onCall(int position) {
        ContactModel contact = modelList.get(position);
        Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:"+contact.getPhone()));
        if (ActivityCompat.checkSelfPermission(activity, Manifest.permission.CALL_PHONE) != PackageManager.PERMISSION_GRANTED) {
            System.out.println("CALLING .... !!!");
            return;
        }

        activity.startActivity(intent);
    }

    static class ViewHolder {
        ImageButton edit;
        ImageButton call;
        TextView name;
        TextView phone;
        ImageView avatar;
    }
}
