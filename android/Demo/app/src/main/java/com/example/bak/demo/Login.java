package com.example.bak.demo;

import android.app.Notification;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Login extends AppCompatActivity implements OnClickListener {

    private Button btnLogin;
    private EditText inputUser;
    private EditText inputPassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        btnLogin = findViewById(R.id.login);
        inputUser = findViewById(R.id.inputUsername);
        inputPassword = findViewById(R.id.inputPassword);
        btnLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.login:
                onLogin();
                break;
            default :
                break;
        }
    }

    private void onLogin() {
        String username = inputUser.getText().toString();

        if (username.isEmpty() || inputPassword.getText().toString().isEmpty()) {
            Toast.makeText(this, "User or password is empty", Toast.LENGTH_LONG).show();
        } else {
            Intent intent = new Intent(this, Profile.class);
            intent.putExtra("user", username);
            startActivity(intent);
        }
    }
}
