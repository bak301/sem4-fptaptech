package com.example.bak.demo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class EditProfile extends AppCompatActivity implements View.OnClickListener{

    private EditText txtUser;
    private Button btnEdit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        txtUser = findViewById(R.id.txtUsername);
        btnEdit = findViewById(R.id.btnEdit);
        btnEdit.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnEdit:
                onEdit();
                break;
            default:
                break;
        }
    }

    private void onEdit() {
        Intent returnIntent = new Intent();
        returnIntent.putExtra("RESULT", txtUser.getText().toString());
        setResult(RESULT_OK, returnIntent);
        finish();
    }
}
