package com.example.bak.demo;

import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class Profile extends AppCompatActivity implements View.OnClickListener {

    private Button buttonEdit;
    private TextView txtUsername;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);
        buttonEdit = findViewById(R.id.btnEdit);
        txtUsername = findViewById(R.id.txtUsername);
        buttonEdit.setOnClickListener(this);

        txtUsername.setText(getIntent().getStringExtra("user"));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnEdit :
                onEdit();
                break;
            default:
                break;
        }
    }

    private void onEdit() {
        //go edit
        Intent intent = new Intent(this, EditProfile.class);
        intent.putExtra("username", txtUsername.getText().toString());
        startActivityForResult(intent, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 1 && resultCode == RESULT_OK) {
            String result = data.getStringExtra("RESULT");
            txtUsername.setText(result);
        }
    }
}
