create database assWEBENT;
use assWEBENT;

create table book (
	id integer primary key auto_increment,
	name varchar(100) not null,
	author varchar(50) not null
)

create table borrowHistory (
	id integer primary key auto_increment,
	book_id integer not null,
	borrow_date datetime not null,
	return_date datetime default '9999-12-31 23:59:59',
	foreign key (book_id) references book(id)
)