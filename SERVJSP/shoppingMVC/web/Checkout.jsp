<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: bak
  Date: 30/05/2018
  Time: 14:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:useBean id="prod" class="model.ProductCart" scope="session"></jsp:useBean>
<html>
<head>
    <title>Title</title>
</head>
<body bgcolor="#FFFFcc">
    <h1>Transaction Details</h1>
    <center>
        <table>
            <tr>
                <td><b>Product Id</b></td>
                <td><b>Product Name</b></td>
                <td><b>Product Type</b></td>
                <td><b>Price</b></td>
                <td><b>Quantity</b></td>
            </tr>

            <c:forEach var="item" items="${prod.cartItems}">
                <tr>
                    <td>${item.productId}</td>
                    <td>${item.productName}</td>
                    <td>${item.productType}</td>
                    <td>${item.price}</td>
                    <td>${item.quantity}</td>
                </tr>
            </c:forEach>
            <tr>
                <td></td>
                <td></td>
                <td><b>Total</b></td>
                <td><b>${prod.amount}</b></td>
                <td></td>
            </tr>
        </table>
        <br>
        <a href="index.jsp">Home</a>
    </center>
</body>
</html>
