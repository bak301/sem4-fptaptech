<%--
  Created by IntelliJ IDEA.
  User: bak
  Date: 28/05/2018
  Time: 16:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
  <head>
    <title>Order Product</title>
  </head>
  <body>
    <h1>Welcome to Shop Stop !!</h1>
    <hr/>

    <jsp:useBean id="prod" class="model.ProductCart" scope="session"/>
    <form action="ShoppingServlet" method="post" name="shoppingForm">
      <b>Products</b> <br/>
      <select name="products">
        <c:forEach var="item" items="${prod.products}">
          <option>
            ${item.productId}${"|"}${item.productName}${"|"}${item.productType}${"|"}${item.price}
          </option>
        </c:forEach>
      </select>

      <br><br>
      <b>Quantity</b><input type="text" name="qty" value="1"><br>
      <input type="hidden" name="action" value="ADD">
      <input type="submit" name="Submit" value="Add to Cart">
    </form>
      <p>${message}</p>
      <jsp:include page="cart.jsp" flush="true"></jsp:include>
  </body>
</html>
