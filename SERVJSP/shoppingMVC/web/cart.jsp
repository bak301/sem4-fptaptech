<%--
  Created by IntelliJ IDEA.
  User: bak
  Date: 30/05/2018
  Time: 14:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<jsp:useBean id="prod" class="model.ProductCart" scope="session"></jsp:useBean>
<center>
    <table bgcolor="lightgreen" border="1" cellspacing="0" cellpadding="0">
        <tr>
            <td><b>Product Id</b></td>
            <td><b>Product Name</b></td>
            <td><b>Product Type</b></td>
            <td><b>Price</b></td>
            <td><b>Quantity</b></td>
            <td> </td>
        </tr>
        <c:forEach var="item" items="${prod.products}">
            <tr>
                <td>${item.productId}</td>
                <td>${item.productName}</td>
                <td>${item.productType}</td>
                <td>${item.price}</td>
                <td>${item.quantity}</td>
                <td>
                    <form action="ShoppingServlet" name="deleteForm" method="post">
                        <input type="submit" value="Delete">
                        <input type="hidden" name="delItem" value="${item.productId}">
                        <input type="hidden" name="action" value="DELETE">
                    </form>
                </td>
            </tr>
        </c:forEach>
    </table>

    <form action="ShoppingServlet" name="checkOutForm" method="post">
        <input type="hidden" name="action" value="CHECKOUT">
        <input type="submit" name="Checkout" value="Checkout">
    </form>
</center>