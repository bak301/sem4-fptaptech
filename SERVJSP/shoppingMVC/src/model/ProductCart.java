package model;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class ProductCart {
    private final List cartItems;

    public ProductCart() {
        cartItems = new ArrayList();
    }

    private List products;
    private float amount;

    public List getProducts(){
        List temp = new ArrayList();
        try
        {
            String dbUser = "bak";
            String dbPass = "rambo";
            String url = "jdbc:mariadb://localhost:3306/shopmvc";
            Class.forName("org.mariadb.jdbc.Driver");
            Connection con = DriverManager.getConnection(url, dbUser, dbPass);
            Statement stm = con.createStatement();
            String sql = "select * from products";
            ResultSet rs = stm.executeQuery(sql);

            while(rs.next()) {
                Product item = new Product();
                item.setProductId(rs.getInt(1));
                item.setProductName(rs.getString(2));
                item.setProductType(rs.getString(3));
                item.setPrice(rs.getFloat(4));

                temp.add(item);
            }
        } catch (SQLException | ClassNotFoundException ex) {
            ex.printStackTrace();
        }

        return temp;
    }

    public void addItem(int id, String name, String type, float price, int quantity) {
        Product item = null;
        boolean match = false;
        for (int i = 0 ; i < cartItems.size() ; i++) {
            if (((Product)cartItems.get(i)).getProductId() == id) {
                item = (Product) cartItems.get(i);
                setAmount(getAmount() + quantity * item.getPrice());
                item.setQuantity(item.getQuantity() + quantity);
                match = true;
                break;
            }
        }

        if (!match) {
            item = new Product (id, name, type, price);
            setAmount(getAmount() + quantity * item.getPrice());
            item.setQuantity(quantity);
            cartItems.add(item);
        }
    }

    public float getAmount() {
        return amount;
    }

    public void setAmount(float amount) {
        this.amount = amount;
    }

    public boolean removeItem(int id) {
        for (Object item :
                cartItems) {
            Product product = (Product) item;
            if (product.getProductId() == id) {
                setAmount(getAmount() - product.getPrice() * product.getQuantity());
                cartItems.remove(((Product) item).getProductId());
                return true;
            }
        }
        return false;
    }
}
