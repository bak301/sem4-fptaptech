package controller;

import model.ProductCart;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.StringTokenizer;

public class ShoppingServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        var session = request.getSession(false);
        if (session == null) response.sendRedirect("shoppingError.jsp");

        var buyList = (ProductCart) session.getAttribute("prod");
        var action = request.getParameter("action");

        if (! "CHECKOUT".equals(action)) {
            if ("DELETE".equals(action))  {
                var productId = Integer.parseInt(request.getParameter("delItem"));
                buyList.removeItem(productId);
            } else if ("ADD".equals(action)) {
                if (buyList == null) buyList = new ProductCart();
                addProduct(request, response, buyList);
            }

            session.setAttribute("prod", buyList);
            var sc = getServletContext();
            var rd = sc.getRequestDispatcher("/index.jsp");
            rd.forward(request, response);
        } else if ("CHECKOUT".equals(action)) {
            var sc = getServletContext();
            var rd = sc.getRequestDispatcher("/checkout.jsp");
            rd.forward(request, response);
        }
    }

    private void addProduct(HttpServletRequest request, HttpServletResponse response, ProductCart obj) {
        String myProd = request.getParameter("products");
        String quantity = request.getParameter("qty");
        try
        {
            if (!validateQuantity(quantity)) {
                var sc = getServletContext();
                var rd = sc.getRequestDispatcher("/shoppingError.jsp");
                request.setAttribute("message", "Quantity should be a positive nonzero value");
                rd.forward(request, response);
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        var t = new StringTokenizer(myProd, "|");
        var productId = new Integer(t.nextToken());
        var productName = t.nextToken();
        var productType = t.nextToken();
        var price = new Float(t.nextToken());

        obj.addItem(productId.intValue(), productName, productType, price.floatValue(), Integer.parseInt(quantity));
    }

    private boolean validateQuantity(String quantity) {
        var valid = false;
        for ( int i = 0 ; i < quantity.length(); i++) {
            if ("0123456789".indexOf(quantity.charAt(i)) >= 0) {
                valid = true;
            } else {
                valid = false;
                break;
            }
        }

        if(!valid) {
            return false;
        } else if (Integer.parseInt(quantity) == 0) {
            return false;
        } else {
            return true;
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
