package DA;

import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;

public final class DbFactory {
    public static SessionFactory factory = new Configuration().configure().buildSessionFactory();
}
