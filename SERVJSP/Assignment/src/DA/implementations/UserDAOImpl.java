package DA.implementations;

import DA.DbFactory;
import DA.interfaces.UserDAO;
import entity.User;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;


public class UserDAOImpl implements UserDAO {

    public UserDAOImpl() {
        this.factory = DbFactory.factory;
    }

    private SessionFactory factory;

    @Override
    public User getUserByNamePass(String name, String pass) {
        String queryString = "FROM User u WHERE u.username = ?1 and u.password = ?2";
        return getUserByProperty(queryString, name, pass);
    }

    @Override
    public User getUserById(int userId) {
        String queryString = "FROM User u where u.id = ?1";
        return getUserByProperty(queryString, userId);

    }

    @Override
    public User getUserByUsername(String name) {
        String queryString = "From User u where u.username = ?1";
        return getUserByProperty(queryString, name);
    }

    @Override
    public boolean createUser(String name, String pass) {
        Session session = factory.openSession();
        Transaction tx = null;

        User user = new User();
        user.setUsername(name);
        user.setPassword(pass);

        int id;

        try {
            tx = session.beginTransaction();
            id = (int) session.save(user);
            tx.commit();
        } catch (HibernateException ex) {
            if (tx != null) tx.rollback();
            return false;
        } finally {
            session.close();
        }

        return id > 0;
    }

    private User getUserByProperty(String queryString, Object... params) {
        Session session = factory.openSession();
        User user = null;
        try {
            Query query = session.createQuery(queryString);
            for (int i = 1 ; i <= params.length ; i++) {
                query.setParameter(i, params[i-1]);
            }

            user = (User) query.getSingleResult();
        } catch (HibernateException ex) {
            ex.printStackTrace();
        } catch (NoResultException ex) {
            //ko co result thi thoi
        } finally {
            session.close();
        }
        return user;
    }
}
