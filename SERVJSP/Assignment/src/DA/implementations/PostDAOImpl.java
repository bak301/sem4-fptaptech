package DA.implementations;

import DA.interfaces.PostDAO;
import entity.Post;

public class PostDAOImpl extends CommonDAOImpl implements PostDAO {

    @Override
    public boolean createPost(Post post) {
        return createEntity(post);
    }

    @Override
    public boolean removePost(Post post) {
        return removeEntity(post);
    }

    @Override
    public boolean updatePost(Post post) {
        return updateEntity(post);
    }
}
