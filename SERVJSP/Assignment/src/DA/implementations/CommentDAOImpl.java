package DA.implementations;

import DA.interfaces.CommentDAO;
import entity.Comment;

public class CommentDAOImpl extends CommonDAOImpl implements CommentDAO {

    @Override
    public boolean createComment(Comment comment) {
        return createEntity(comment);
    }

    @Override
    public boolean removeComment(Comment comment) {

        return removeEntity(comment);
    }

    @Override
    public boolean updateComment(Comment comment) {
        return updateEntity(comment);
    }
}
