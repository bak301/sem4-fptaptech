package DA.implementations;

import DA.DbFactory;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class CommonDAOImpl {

    protected SessionFactory factory;

    public CommonDAOImpl() {
        this.factory = DbFactory.factory;
    }

    public <T> boolean createEntity(T t){
        return action(t, "save");
    }

    public <T> boolean removeEntity(T t) {
        return action(t,"delete");
    }

    public <T> boolean updateEntity(T t) {
        return action(t, "update");
    }

    private <T> boolean action(T t, String methodName) {
        Session session = factory.openSession();
        Transaction tx = null;
        boolean isSuccess = false;
        try {
            tx = session.beginTransaction();
            Method m = session.getClass().getMethod(methodName, Object.class);
            m.invoke(session, t);
            tx.commit();
            isSuccess = true;
        } catch (HibernateException ex) {
            if (tx != null) tx.rollback();
        } catch (NoSuchMethodException | IllegalAccessException | InvocationTargetException e) {
            e.printStackTrace();
        } finally {
            session.close();
        }
        return isSuccess;
    }
}
