package DA.interfaces;

import entity.Comment;

public interface CommentDAO {
    boolean createComment(Comment comment);
    boolean removeComment(Comment comment);
    boolean updateComment(Comment comment);
}
