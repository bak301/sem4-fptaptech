package DA.interfaces;

import entity.Post;

public interface PostDAO {
    boolean createPost(Post post);
    boolean removePost(Post post);
    boolean updatePost(Post post);
}
