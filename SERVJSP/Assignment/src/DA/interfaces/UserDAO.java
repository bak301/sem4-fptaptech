package DA.interfaces;

import entity.User;

public interface UserDAO {
    User getUserByNamePass(String name, String pass);
    User getUserById(int userId);
    User getUserByUsername(String name);

    boolean createUser(String name, String pass);
}

