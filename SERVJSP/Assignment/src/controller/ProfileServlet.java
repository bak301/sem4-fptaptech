package controller;

import entity.Comment;
import entity.Post;
import entity.User;
import model.CommentBean;
import model.PostBean;
import model.UserBean;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Timestamp;
import java.time.LocalDateTime;

public class ProfileServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        boolean isSuccess = false;
        String action = request.getParameter("action");

        if ("post".equals(action)) {
            isSuccess = post(request);
        } else if ("comment".equals(action)) {
            isSuccess = comment(request);
        }

        request.setAttribute("status", isSuccess);
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String[] urlComponents = request.getRequestURI().split("/");
        String username = urlComponents[urlComponents.length - 1];

        if (!"profile".equals(username)) {
            User user = new UserBean(username).getUser();
            request.setAttribute("user", user);
        } else {
            request.setAttribute("user", request.getSession(false).getAttribute("user"));
        }

        request.getRequestDispatcher(request.getContextPath() + "/app/profile.jsp").forward(request, response);
    }

    // Private method
    private boolean comment(HttpServletRequest req) {
        int postId = Integer.valueOf(req.getParameter("postId"));
        int userId = Integer.valueOf(req.getParameter("userId"));
        String content = req.getParameter("content");
        Timestamp date = Timestamp.valueOf(LocalDateTime.now());

        Comment comment = new Comment(postId, userId, content, date);
        return new CommentBean(comment).comment();
    }

    private boolean post(HttpServletRequest req) {
        int userId = Integer.valueOf(req.getParameter("userId"));
        String content = req.getParameter("content");
        Timestamp date = Timestamp.valueOf(LocalDateTime.now());

        Post post = new Post(userId, content, date);
        return new PostBean(post).post();
    }
}
