package controller;

import entity.Comment;
import entity.Post;
import model.CommentBean;
import model.PostBean;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class RemoveServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        int id = Integer.valueOf(request.getParameter("id"));
        String entityType = request.getParameter("type");
        String user = request.getParameter("user");

        if ("comment".equals(entityType)) {
            Comment comment = new Comment(id);
            new CommentBean(comment).removeComment();
        } else if ("post".equals(entityType)) {
            Post post = new Post(id);
            new PostBean(post).removePost();
        }

        response.sendRedirect(request.getContextPath() + "/app/profile/" + user);
    }
}
