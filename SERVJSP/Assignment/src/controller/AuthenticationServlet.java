package controller;

import model.UserBean;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class AuthenticationServlet extends HttpServlet {
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String user = req.getParameter("user");
        String pass = req.getParameter("pass");

        UserBean bean = new UserBean();

        if (bean.login(user, pass)) {
            req.getSession().setAttribute("user", bean.getUser());

            if ("admin".equals(user)) {
                resp.sendRedirect("app/admin");
                return;
            }

            resp.sendRedirect("app/profile/" + user);
        } else {
            req.getRequestDispatcher("login.jsp").forward(req, resp);
        }

    }

    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String path = req.getServletPath();

        if (path.contains("/logout")) {
            req.getSession().invalidate();
            req.setAttribute("logout", "You are logged out !");
            req.getRequestDispatcher("index.jsp").forward(req, resp);

            return;
        } else {
            req.getRequestDispatcher("login.jsp").forward(req, resp);
        }
    }
}
