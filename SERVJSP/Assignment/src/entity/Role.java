package entity;

public enum Role {
    GUEST,
    ADMIN,
    USER;
}
