package model;

import DA.implementations.PostDAOImpl;
import DA.interfaces.PostDAO;
import entity.Post;

public class PostBean {
    private Post post;
    private PostDAO dao;

    public PostBean(Post post) {
        this.post = post;
        dao = new PostDAOImpl();
    }

    public Post getPost() {
        return post;
    }

    public void setPost(Post post) {
        this.post = post;
    }

    public boolean post(){
        return dao.createPost(post);
    }

    public boolean removePost() {
        return dao.removePost(post);
    }
}
