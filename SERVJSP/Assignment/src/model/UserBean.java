package model;

import DA.implementations.UserDAOImpl;
import DA.interfaces.UserDAO;
import entity.User;

public class UserBean {
    private User user;
    private UserDAO dao;


    private void init() {
        dao = new UserDAOImpl();
    }

    public UserBean() {
        init();
    }

    public UserBean(String name) {
        init();
        this.user = dao.getUserByUsername(name);
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public boolean login(String name, String pass) {
        this.user = dao.getUserByNamePass(name, pass);
        return user!= null;
    }

    public boolean register(String username, String pass) {
        return dao.createUser(username, pass);
    }
}
