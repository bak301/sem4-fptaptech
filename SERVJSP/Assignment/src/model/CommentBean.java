package model;

import DA.DbFactory;
import DA.implementations.CommentDAOImpl;
import DA.interfaces.CommentDAO;
import entity.Comment;

import java.io.Serializable;

public class CommentBean implements Serializable {
    private Comment comment;
    private CommentDAO dao;

    public CommentBean() {
        init();
    }

    public CommentBean(Comment comment) {
        init();
        this.comment = comment;
    }

    private void init() {
        dao = new CommentDAOImpl();
    }

    public Comment getComment() {
        return comment;
    }

    public void setComment(Comment comment) {
        this.comment = comment;
    }

    public boolean comment() {
        return dao.createComment(comment);
    }

    public boolean removeComment() {
        return dao.removeComment(comment);
    }
}
