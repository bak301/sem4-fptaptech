<%--
  Created by IntelliJ IDEA.
  User: bak
  Date: 11/06/2018
  Time: 16:21
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" session="false" %>
<nav class="row">
    <div class="col-md-6">
        <a class="btn btn-info" href="${param.contextPath}/">Home</a>
        <a class="btn btn-info" href="${param.contextPath}/login">Login</a>
        <a class="btn btn-info" href="${param.contextPath}/app/profile/${param.user}">Your Profile</a>
        <a class="btn btn-info" href="${param.contextPath}/logout">Logout</a>
    </div>

    <div class="col-md-3">
        <h3>S NET</h3>
    </div>

    <div class="col-md-3">
        <textarea class="form-control" name="searchBar" id="searchBar" cols="10" rows="1" placeholder="... Go to User page ..." onkeydown="search()"></textarea>
    </div>

    <script>
        function search() {
            var key = window.event.keyCode;

            if (key == 13) {
                var searchBox = document.getElementById("searchBar");
                var nextUrl = "${param.contextPath}/app/profile/" + searchBox.value;
                window.location.replace(nextUrl);
            }
        }
    </script>
</nav>
