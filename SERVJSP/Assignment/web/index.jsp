<%--
  Created by IntelliJ IDEA.
  User: bak
  Date: 05/06/2018
  Time: 01:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" session="false" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
  <head>
    <title>Index page</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
  </head>
  <body>
    <div class="container">
      <jsp:include page="navigation.jsp" flush="true"/>
      Welcome to S NET!
      <c:if test="${requestScope.logout != null}">
        <h2>${requestScope.logout}</h2>
      </c:if>
    </div>
  </body>
</html>
