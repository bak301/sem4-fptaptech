$(document).ready(function () {
    $(function () {
        $('#contentArea').summernote();
    });

    $('.toggleCollapse').click(function () {
        $(this).next('.collapse').collapse('toggle');
    });
})