<%--
  Created by IntelliJ IDEA.
  User: bak
  Date: 06/06/2018
  Time: 15:07
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" session="false" %>
<html>
<head>
    <title>Admin page</title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-WskhaSGFgHYWDcbwN70/dfYBj47jz9qbsMId/iRN3ewGhXQFZCSftd1LZCfmhktB" crossorigin="anonymous">
    <link rel="stylesheet" href="../css/profile.css">
</head>
<body>
    <div class="container">
        <jsp:include page="../navigation.jsp" flush="true"/>
        <form action="admin" method="post">
            <header>
                <h1>Create account</h1>
            </header>

            <div class="form-group">
                <label for="username">Username : </label>
                <input id="username" type="text" name="username">
            </div>

            <div class="form-group">
                <label for="password">Password: </label>
                <input id="password" type="password" name="password">
            </div>

            <input type="submit" value="Create account">
        </form>
    </div>
</body>
</html>
