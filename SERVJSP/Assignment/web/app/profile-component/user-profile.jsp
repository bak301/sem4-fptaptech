<%--
  Created by IntelliJ IDEA.
  User: bak
  Date: 13/06/2018
  Time: 15:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<header class="jumbotron">
    <h1>${user.username}'s page</h1>
</header>

<c:set var="isUserOnHisPage" value="${user.username.equals(sessionScope.user.username)}" scope="request"/>

<main class="row">
    <div class="col-md-5"></div>
    <div id="timeline" class="col-md-7">

        <c:if test="${isUserOnHisPage}">
            <section id="new-post" class="card post-wrapper">
                <div class="card-header"></div>
                <div class="card-body">
                    <form method="post">
                        <div class="form-group">
                            <input type="hidden" name="user" value="${user.username}">
                            <input type="hidden" name="userId" value="${sessionScope.user.id}">
                            <input type="hidden" name="action" value="post">
                            <!-- <input class="form-control" type="text" name="content" placeholder="... What are you thinking now ? ..."> -->
                            <textarea name="content" id="contentArea" cols="30" rows="10" placeholder="... What are you thinking .. ? "></textarea>
                        </div>
                        <input class="btn btn-primary form-control" type="submit" value="Post Status">
                    </form>
                </div>
                <div class="card-footer"></div>
            </section>
        </c:if>

        <section id="user-posts">
            <c:forEach items="${user.postsById}" var="post">
                <div class="card post-wrapper">

                    <div class="post card-body">
                        <div class="row">
                            <div class="col-md-6">
                                <h3 class="card-title">${post.userByUserId.username}</h3>
                            </div>

                            <div class="col-md-3"></div>

                            <div class="col-md-3">
                                <c:if test="${isUserOnHisPage}">
                                    <form action="remove" method="post">
                                        <input type="hidden" name="id" value="${post.id}">
                                        <input type="hidden" name="type" value="post">
                                        <input type="hidden" name="user" value="${user.username}">
                                        <button type="submit"><i class="fas fa-trash"></i></button>
                                    </form>
                                </c:if>
                            </div>
                        </div>

                        <div class="card-text">
                            <i>${post.content}</i>
                        </div>
                    </div>

                    <%--Comments Collection--%>
                    <a class="btn btn-info toggleCollapse">Show comments</a>

                    <ul class="list-group list-group-flush collapse">
                        <c:forEach items="${post.commentsById}" var="comment">
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-lg-11">
                                        <b>${comment.userByUserId.username}</b>: ${comment.content}
                                    </div>

                                    <c:if test="${comment.userByUserId.username.equals(sessionScope.user.username)}">
                                        <div class="col-lg-1">
                                            <form action="remove" method="post">
                                                <input type="hidden" name="id" value="${comment.id}">
                                                <input type="hidden" name="type" value="comment">
                                                <input type="hidden" name="user" value="${user.username}">
                                                <button type="submit"><i class="fas fa-trash"></i></button>
                                            </form>
                                        </div>
                                    </c:if>
                                </div>
                            </li>
                        </c:forEach>
                    </ul>
                    <%--End of Comment Collection--%>

                    <c:if test="${sessionScope.user != null}">
                        <section class="card-footer add-comment">
                            <form method="post">
                                <div class="form-group row">
                                    <input type="hidden" name="action" value="comment">
                                    <input type="hidden" name="user" value="${user.username}">
                                    <input type="hidden" name="postId" value="${post.id}">
                                    <input type="hidden" name="userId" value="${sessionScope.user.id}">

                                    <label for="commentContent" class="label col-md-3">${sessionScope.user.username}</label>
                                    <input id="commentContent" class="form-control col-md-9" type="text" name="content">
                                    <input class="hidden-input" type="submit" value="comment">
                                </div>
                            </form>
                        </section>

                    </c:if>
                </div>
            </c:forEach>
        </section>
    </div>
</main>
