create database if not exists snet;
use snet;

create table User (
id int primary key auto_increment,
username varchar(100) not null unique,
password varchar(100) not null,
role smallint default 2);

create table Post (
id int primary key auto_increment,
user_id int not null,
content varchar(1000),
date datetime not null,
foreign key (user_id) references User(id));

create table Comment (
id int primary key auto_increment,
user_id int not null,
post_id int not null,
content varchar(1000),
date datetime not null,
foreign key (user_id) references User(id),
foreign key (post_id) references Post(id));