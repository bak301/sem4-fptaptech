package controller;

import entity.Phone;
import model.PhoneBean;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

public class PhoneServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String name = request.getParameter("name");
        String brand = request.getParameter("brand");
        float price = Float.parseFloat(request.getParameter("price"));
        String description = request.getParameter("description");

        Phone phone = new Phone(name, brand, price, description);
        PhoneBean bean = new PhoneBean();
        bean.setPhone(phone);

        boolean result = bean.addPhone();
        request.getRequestDispatcher("listphone.jsp").forward(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("listphone.jsp").forward(request, response);
    }
}
