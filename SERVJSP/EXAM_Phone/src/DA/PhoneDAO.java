package DA;

import entity.Phone;
import org.hibernate.HibernateException;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.util.ArrayList;
import java.util.List;

public class PhoneDAO {

    private SessionFactory factory;

    public PhoneDAO() {
        factory = new Configuration().configure().buildSessionFactory();
    }

    public boolean createPhone(Phone phone) {
        Session session = factory.openSession();
        Transaction tx = null;
        boolean result;
        try {
            tx = session.beginTransaction();
            result = (int)session.save(phone) > 0;
            tx.commit();
        } catch (HibernateException ex) {
            if (tx != null) tx.rollback();
            result = false;
            ex.printStackTrace();
        } finally {
            session.close();
        }

        return result;
    }

    public List<Phone> getPhones() {
        Session session = factory.openSession();
        List<Phone> result;
        try {
            Query query = session.createQuery("SELECT p from Phone p");
            result = query.getResultList();
        } catch (HibernateException ex) {
            result = new ArrayList<>();
        } finally {
            session.close();
        }
        return result;
    }
}
