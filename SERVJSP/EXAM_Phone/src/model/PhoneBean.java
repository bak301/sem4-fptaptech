package model;

import DA.PhoneDAO;
import entity.Phone;

import java.io.Serializable;
import java.util.List;

public class PhoneBean implements Serializable {
    private Phone phone;
    private PhoneDAO dao;

    public PhoneBean(){
        dao = new PhoneDAO();
    }

    public Phone getPhone() {
        return phone;
    }

    public void setPhone(Phone phone) {
        this.phone = phone;
    }

    public boolean addPhone() {
        return dao.createPhone(phone);
    }

    public List<Phone> getPhoneList() {
        return dao.getPhones();
    }
}
