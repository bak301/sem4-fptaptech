package entity;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Cacheable(value = false)
public class Phone {
    private int id;
    private String name;
    private String brand;
    private Float price;
    private String description;

    public Phone() {

    }

    public Phone(String name, String brand, float price, String description) {
        this.name = name;
        this.brand = brand;
        this.price = price;
        this.description = description;
    }

    @Id
    @Column(name = "id", nullable = false)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Basic
    @Column(name = "name", nullable = false, length = 100)
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Basic
    @Column(name = "brand", nullable = false, length = 10)
    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    @Basic
    @Column(name = "price", nullable = true, precision = 0)
    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    @Basic
    @Column(name = "description", nullable = true, length = 500)
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Phone phone = (Phone) o;
        return id == phone.id &&
                Objects.equals(name, phone.name) &&
                Objects.equals(brand, phone.brand) &&
                Objects.equals(price, phone.price) &&
                Objects.equals(description, phone.description);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, name, brand, price, description);
    }
}
