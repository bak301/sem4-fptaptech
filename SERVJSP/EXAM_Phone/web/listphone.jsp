<%--
  Created by IntelliJ IDEA.
  User: bak
  Date: 18/06/2018
  Time: 14:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <title>List phone</title>
</head>
<body>
    <jsp:useBean id="bean" class="model.PhoneBean" scope="request"/>

    <a href="addphone.jsp">Add phone</a>

    <table border="1">
        <thead>
            <tr>
                <th>Name</th>
                <th>Brand</th>
                <th>Price</th>
                <th>Description</th>
            </tr>
        </thead>
        <tbody>
            <c:forEach items="${bean.phoneList}" var="phone">
                <tr>
                    <td>${phone.name}</td>
                    <td>${phone.brand}</td>
                    <td>${phone.price}</td>
                    <td>${phone.description}</td>
                </tr>
            </c:forEach>
        </tbody>
    </table>
</body>
</html>
