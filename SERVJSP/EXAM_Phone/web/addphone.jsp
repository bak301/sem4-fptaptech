<%--
  Created by IntelliJ IDEA.
  User: bak
  Date: 18/06/2018
  Time: 14:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Add phone</title>
</head>
<body>
    <form action="phone" method="post">
        Name : <input type="text" name="name" required maxlength="50"><br>
        Brand :
        <select name="brand" id="brand" required>
            <option value="Apple">Apple</option>
            <option value="Samsung">Samsung</option>1
            <option value="Nokia">Nokia</option>
            <option value="Others">Other</option>
        </select><br>
        Price : <input type="number" name="price" step="0.01" required><br>
        Description : <textarea name="description" id="description" cols="30" rows="4" maxlength="500"></textarea><br>

        <input type="reset">
        <input type="submit" value="Add Phone">
    </form>
</body>
</html>
