<%@ page import="java.HRManager.entities.Employee" %>
<%@ page import="java.HRManager.ConvertData" %>
<%@page contentType="text/html" pageEncoding="UTF-8" %>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
"http://www.w3.org/TR/html4/loose.dtd">

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>Employees Manager</title>
    <link href="css/menu_style.css" type="text/css" rel="stylesheet"/>
    <link href="css/style.css" type="text/css" rel="stylesheet"/>
</head>
<body>
<c:choose>
<c:when test="${sessionScope.username != null}">
<div class="header">
    <br>
    <h1 align="center">Employees Manager</h1>
    <div class="menu bubplastic horizontal orange">
        <ul>
            <li><span class="menu_r"><a href="login.jsp"><span class="menu_ar">Login</span></a></span></li>
            <li class="highlight"><span class="menu_r"><a href="employeeManager.jsp"><span class="menu_ar">Employee Manager</span></a></span>
            </li>
            <li><span class="menu_r"><a href="ProcessEmployee"><span class="menu_ar">Add New Employee</span></a></span>
            </li>
            <li><span class="menu_r"><a href="searchEmployee.jsp"><span
                    class="menu_ar">Search Employee</span></a></span></li>
            <li><span class="menu_r"><a href="logout.jsp"><span class="menu_ar">logout</span></a></span></li>
        </ul>
        <br class="clearit"/>
    </div>
</div>
<div class="content">
    <br><br>
    <table id="tb" width="100%" align="center">
        <tr>
            <th colspan="11" height="50px"><h4>EMPLOYEE LIST</h4></th>
        </tr>
        <tr>
            <th>Name</th>
            <th>Birthday Date</th>
            <th>Hire Date</th>
            <th>Address</th>
            <th>City</th>
            <th>Country</th>
            <th>Home Phone</th>
            <th>Mobile</th>
            <th>Email</th>
            <th colspan="2"></th>
        </tr>
        <jsp:useBean id="ebo" class="java.HRManager.bol.EmployeeBO" scope="request"/>

        <c:set var="option" value="${param.option}"/>
        <c:set var="value" value="${param.value}"/>
        <c:choose>
            <c:when test="${option == null || value == null}">
                <c:set var="arr" value="${ebo.select()}"/>
            </c:when>

            <c:otherwise>
                <c:if test="${\"Name\".equals(option)}">
                    <c:set var="arr" value="${ebo.find(0, value)}"/>
                </c:if>

                <c:if test="${\"City\".equals(option)}">
                    <c:set var="arr" value="${ebo.find(1, value)}"/>
                </c:if>
            </c:otherwise>
        </c:choose>

        <jsp:useBean id="convert" beanName="convert" class="java.HRManager.ConvertData"/>
        <c:if test="${arr.length > 0}">
            <c:forEach var="item" items="${arr}">
                <tr>
                    <td>${item.firstName} ${item.lastName}</td>
                    <td>${convert.date2string(item.birthDate)}</td>
                    <td>${convert.date2string(item.hireDate)}</td>
                    <td>${item.adress}</td>
                    <td>${item.city}</td>
                    <td>${item.country}</td>
                    <td>${item.homePhone}</td>
                    <td>${item.mobile}</td>
                    <td>${item.email}</td>
                    <td align="center"><a href="ProcessEmployee?action=edit&id=${item.employeeID}">Edit</a></td>
                    <td align="center"><a href="ProcessEmployee?action=delete&id=${item.employeeID}">Delete</a></td>
                </tr>
            </c:forEach>
        </c:if>
    </table>
    <br>
    </c:when>

    <c:otherwise>
        <jsp:forward page="login.jsp"/>
    </c:otherwise>
    </c:choose>
</div>
</body>
</html>