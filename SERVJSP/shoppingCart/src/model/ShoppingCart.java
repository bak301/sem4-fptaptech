package model;

import java.util.LinkedList;
import java.util.List;

public class ShoppingCart {

    private List<String> list = new LinkedList<>();

    public void setProduct(String name) {
        list.add(name);
    }

    public List<String> getList() {
        return list;
    }
}
