<%--
  Created by IntelliJ IDEA.
  User: bak
  Date: 06/06/2018
  Time: 16:44
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>cart</title>
</head>
<body>
    <jsp:useBean id="cart" class="model.ShoppingCart" scope="session"/>
    <jsp:setProperty name="cart" property="product" param="name"/>
    <h1>Cart : </h1>
    <jsp:getProperty name="cart" property="list"/>
</body>
</html>
