<%--
  Created by IntelliJ IDEA.
  User: bak
  Date: 16/07/2018
  Time: 15:42
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Authentication</title>
</head>
<body>
<form action="authenticate">
    User : <input type="text" name="username">
    <br>
    Password : <input type="password" name="password">
    <br>

    <input type="submit" value="Log in">
    <button onclick="login()">JS login</button>
</form>

<script>
    function login() {
        fetch('authenticate' , {
            method: 'PUT',
            headers: {'Content-Type':'application/x-www-form-urlencoded'},
            body: new URLSearchParams(`username={document.querySelector('input[name="username"]')}&password={document.querySelector('input[name="password"]')}`)
        })
    }
</script>
</body>
</html>
