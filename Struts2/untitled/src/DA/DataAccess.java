package DA;

import entity.User;

import java.sql.*;

public class DataAccess {
    private Connection con;

    public DataAccess() throws SQLException, ClassNotFoundException {
        Class.forName("org.mariadb.jdbc.Driver");
        con = DriverManager.getConnection("jdbc:mariadb://localhost:3306/auth?user=bak&password=rambo");
    }

    public boolean authenticate(User user) {
        try {
            String query = "select * from account where username=? and password=?";
            PreparedStatement stm = con.prepareStatement(query);
            stm.setString(1, user.getUsername());
            stm.setString(2, user.getPassword());
            ResultSet rs = stm.executeQuery();
            boolean result = rs.next();
            rs.close();
            stm.close();
            con.close();

            return result;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        }
    }
}
