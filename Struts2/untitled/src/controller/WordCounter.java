package controller;

import com.opensymphony.xwork2.ActionSupport;

public class WordCounter extends ActionSupport {
    private String text;
    private int count;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getText() {

        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String execute() throws Exception {
        String text = this.text.trim();
        if (text.isEmpty()) return ERROR;

        count = text.split(" ").length;
        return SUCCESS;
    }
}
