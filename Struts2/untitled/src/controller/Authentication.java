package controller;

import DA.DataAccess;
import com.opensymphony.xwork2.ActionSupport;
import entity.User;
import org.apache.struts2.interceptor.ServletRequestAware;

import javax.servlet.http.HttpServletRequest;

public class Authentication extends ActionSupport implements ServletRequestAware {
    private String username;
    private String password;
    private HttpServletRequest req;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String execute() throws Exception {
        System.out.println(this.req.getMethod() + ": " + req.getRequestURL());
        User user = new User(username, password);
        return new DataAccess().authenticate(user) ? SUCCESS : ERROR;
    }

    @Override
    public void setServletRequest(HttpServletRequest httpServletRequest) {
        this.req = httpServletRequest;
    }
}
