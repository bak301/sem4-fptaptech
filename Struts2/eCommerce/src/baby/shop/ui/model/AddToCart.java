package baby.shop.ui.model;

import baby.shop.biz.Cart;
import baby.shop.da.ProductManager;
import baby.shop.entity.Product;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import java.util.Map;

public class AddToCart extends ActionSupport {
    private int newProductId;

    public void setNewProductId(int id) {
        this.newProductId = id;
    }

    @Override
    public String execute() throws Exception {
//        Map<String , Object> session = ActionContext.getContext().getSession();
        Cart cart = (Cart) ActionContext.getContext().getSession().get("cart");
        if (cart == null) {
            cart = new Cart();
        }

        Product p = new ProductManager().getProductById(newProductId);
        if (p.getId() == 0) {
            return ERROR;
        } else {
            cart.addProduct(p);
            ActionContext.getContext().getSession().put("cart", cart);
            return SUCCESS;
        }
    }
}
