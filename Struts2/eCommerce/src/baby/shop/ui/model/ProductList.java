package baby.shop.ui.model;

import baby.shop.da.ProductManager;
import baby.shop.entity.Product;
import com.opensymphony.xwork2.ActionSupport;

import java.util.List;

public class ProductList extends ActionSupport {
    private String keyword;
    private List<Product> products;

    @Override
    public String execute() throws Exception {
        products = new ProductManager().getProductsByName(keyword);
        return SUCCESS;
    }

    public List<Product> getProducts() {
        return products;
    }

    public String getKeyword() {

        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

}
