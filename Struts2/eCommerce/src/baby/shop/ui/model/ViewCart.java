package baby.shop.ui.model;

import baby.shop.biz.Cart;
import baby.shop.entity.Product;
import com.opensymphony.xwork2.ActionContext;
import com.opensymphony.xwork2.ActionSupport;

import java.util.Map;

public class ViewCart extends ActionSupport {
    private Map<Product, Integer> products;
    private float total;

    public Map<Product, Integer> getProducts() {
        return products;
    }

    public float getTotal() {
        return total;
    }

    @Override
    public String execute() throws Exception {
        Cart cart = (Cart) ActionContext.getContext().getSession().get("cart");
        if (cart == null) {
            return ERROR;
        } else {
            products = cart.getProducts();
            total = cart.getTotalPrice();
            return SUCCESS;
        }
    }
}
