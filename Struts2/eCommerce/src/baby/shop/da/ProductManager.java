package baby.shop.da;

import baby.shop.entity.Product;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ProductManager {
    private static PreparedStatement searchByNameStatement;
    private static PreparedStatement searchByIdStatement;

    public PreparedStatement getSearchByNameStatement() throws ClassNotFoundException, SQLException {
        if (searchByNameStatement == null) {
            Connection con = DBConnection.getConnection();

            searchByNameStatement = con.prepareStatement("select id, name, price, description from product where name like ?");
        }

        return searchByNameStatement;
    }

    public PreparedStatement getSearchByIdStatement() throws ClassNotFoundException, SQLException {
        if (searchByIdStatement == null) {
            Connection con = DBConnection.getConnection();

            searchByIdStatement = con.prepareStatement("select id, name, price, description from product where id = ? ");
        }

        return searchByIdStatement;
    }

    public List<Product> getProductsByName(String keyword) {
        try {
            PreparedStatement statement = getSearchByNameStatement();
            statement.setString(1, "%" + keyword + "%");
            ResultSet rs = statement.executeQuery();
            List<Product> products = new LinkedList<>();
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                float price = rs.getFloat("price");
                String description = rs.getString("description");

                products.add(new Product(id, name, price, description));
            }
            return products;
        } catch (ClassNotFoundException | SQLException e) {
            Logger.getLogger(ProductManager.class.getName()).log(Level.SEVERE, null, e);
            return new LinkedList<>();
        }
    }

    public Product getProductById(int id){
        try {
            PreparedStatement statement = getSearchByIdStatement();
            statement.setInt(1, id);
            ResultSet rs = statement.executeQuery();
            if (rs.next()) {
                String name = rs.getString("name");
                float price = rs.getFloat("price");
                String description = rs.getString("description");

                return new Product(id, name, price, description);
            }
        } catch (ClassNotFoundException | SQLException e) {
            Logger.getLogger(ProductManager.class.getName()).log(Level.SEVERE, null, e);
        }
        return new Product(0, "", 0 , "");
    }
}
