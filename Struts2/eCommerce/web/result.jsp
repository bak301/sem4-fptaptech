<%--
  Created by IntelliJ IDEA.
  User: bak
  Date: 18/07/2018
  Time: 14:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib prefix="s" uri="/struts-tags" %>
<html>
<head>
    <title>Product list of search : <s:property value="keyword"/></title>
</head>
<body>
    <h1>Product list of search : <s:property value="keyword"/></h1>
    <table>
        <s:iterator value="products" var="product">
            <tr>
                <td><s:property value="name"/></td>
                <td><s:property value="price"/></td>
                <td><s:property value="description"/></td>
                <td><a href="addToCart?newProductId=<s:property value="id"/>">Add to cart</a></td>
            </tr>
        </s:iterator>
        <a href="index.jsp">Back</a>
    </table>
</body>
</html>
