<%@ taglib prefix="s" uri="/struts-tags" %>
<%--
  Created by IntelliJ IDEA.
  User: bak
  Date: 18/07/2018
  Time: 14:30
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Cart Details</title>
</head>
<body>
    <h1>Cart Details</h1>
    <table border="1">
        <tr>
            <th>Product</th>
            <th>Amount</th>
        </tr>

        <s:iterator value="products" var="product">
            <tr>
                <td><s:property value="key.name"/></td>
                <td><s:property value="value"/><br></td>
            </tr>
        </s:iterator>
    </table>
    <label>Total: </label>
    <s:property value="total"/><br>
    <a href="index.jsp">Continue buying</a>
</body>
</html>
