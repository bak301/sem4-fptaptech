public class Student {
    private String sex;
    private String name;
    private String address;
    private String phone;

    public Student(String sex, String name, String address, String phone) {
        this.sex = sex;
        this.name = name;
        this.address = address;
        this.phone = phone;
    }

    public Student() {
    }

    public String getSex() {
        return sex;
    }

    public String getName() {
        return name;
    }

    public String getAddress() {
        return address;
    }

    public String getPhone() {
        return phone;
    }

}
