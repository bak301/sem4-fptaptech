import com.sun.istack.internal.NotNull;
import org.w3c.dom.Attr;
import org.w3c.dom.Comment;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;

public class XMLStudent {

    private Document doc;
    private Transformer transformer;
    private File file;

    public XMLStudent(Document doc, Transformer transformer, File file) {
        this.doc = doc;
        this.transformer = transformer;
        this.file = file;
    }

    public void initDocument() {
        Element root = doc.createElement("students");
        Student firstStudent = new Student(
                "female",
                "Phan Thuy Duong",
                "Hai Phong",
                "0999123334"
        );

        Student secondStudent = new Student(
                "male",
                "Nguyen Thanh Long",
                "Hong Kong",
                "9733453346"
        );

        root.appendChild(newStudentElement(firstStudent));
        root.appendChild(newStudentElement(secondStudent));
        doc.appendChild(root);
        saveChanges("Init Document");
    }

    public void insertInMiddle(Student middleStudent) {
        Element root = doc.getDocumentElement();
        Element middleStudentElement = newStudentElement(middleStudent);
        Element lastElement = (Element) root.getLastChild();

        root.insertBefore(middleStudentElement, lastElement);

        saveChanges("Insert in Middle");
    }

    public void createCommentBeforeFirstStudent(String content) {
        Element root = doc.getDocumentElement();
        Comment comment = doc.createComment(content);
        Element firstStudent = (Element) root.getFirstChild();

        root.insertBefore(comment, firstStudent);

        saveChanges("Create Comment");
    }

    public void removeLastStudent() {
        Element root = doc.getDocumentElement();

        root.removeChild(root.getLastChild());

        saveChanges("Remove Last Student");
    }

    private void saveChanges(String change) {
        DOMSource source = new DOMSource(doc);
        StreamResult fileResult = new StreamResult(file);

        //Console test
        System.out.println("\n ----------------" + change.toUpperCase() + "------------------\n");
        StreamResult consoleResult = new StreamResult(System.out);
        try {
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

            transformer.transform(source, consoleResult);

            //Write to file
            transformer.transform(source, fileResult);
        } catch (TransformerException e) {
            e.printStackTrace();
        }
    }

    //Internal method
    private Element newStudentElement(@NotNull Student student){
        Element studentElement = doc.createElement("student");

        Attr sex = doc.createAttribute("sex");
        sex.setValue(student.getSex());
        studentElement.setAttributeNode(sex);

        Element name = doc.createElement("name");
        name.setTextContent(student.getName());
        studentElement.appendChild(name);

        Element address = doc.createElement("address");
        address.setTextContent(student.getAddress());
        studentElement.appendChild(address);

        Element phone = doc.createElement("phone");
        phone.setTextContent(student.getPhone());
        studentElement.appendChild(phone);

        return studentElement;
    }
}
