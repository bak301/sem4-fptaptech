import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerFactory;
import java.io.File;
import java.io.IOException;

public class Main {

    private static Document doc;
    private static Transformer transformer;

    public static void main(String[] args) throws IOException {
        File file = new File("student.xml");
        doc = getDoc();
        transformer = getTransformer();

        XMLStudent xmlStudent = new XMLStudent(doc, transformer, file);

        xmlStudent.initDocument();
        System.in.read();
        xmlStudent.insertInMiddle(new Student(
                "gay",
                "jay",
                "earth",
                "009909090"
        ));
        System.in.read();
        xmlStudent.createCommentBeforeFirstStudent("Hello Student !");
        System.in.read();
        xmlStudent.removeLastStudent();
        System.in.read();
    }

    // Getter

    private static Document getDoc() {
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;
        try {
            builder = documentBuilderFactory.newDocumentBuilder();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        return builder.newDocument();
    }

    private static Transformer getTransformer() {
        try {
            return TransformerFactory.newInstance().newTransformer();
        } catch (TransformerConfigurationException e) {
            e.printStackTrace();
        }
        return null;
    }
}
