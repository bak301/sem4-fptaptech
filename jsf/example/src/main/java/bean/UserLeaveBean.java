package bean;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.awt.event.ActionEvent;
import java.io.Serializable;
import java.time.LocalDate;

@Named(value = "userLeaveBean")
@SessionScoped
public class UserLeaveBean implements Serializable {
    private String name;
    private String reason;
    private LocalDate date;

    private String message;

    public UserLeaveBean() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String apply(ActionEvent event) {
        this.message = "Your leave form has been sent. You should wait for your admin to confirm it";
        return "success";
    }
}
