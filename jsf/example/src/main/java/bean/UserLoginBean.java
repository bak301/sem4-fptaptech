package bean;

import javax.enterprise.context.SessionScoped;
import javax.inject.Named;
import java.awt.event.ActionEvent;
import java.io.Serializable;

/**
 *
 * @author tuann
 */
@Named(value = "userLoginBean")
@SessionScoped
public class UserLoginBean implements Serializable {

    /**
     * Creates a new instance of UserLoginBean
     */
    private String username;
    private String password;
    private String message = "Please enter your username or password";

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public UserLoginBean() {
    }

    public String login(ActionEvent event)
    {
        boolean isManager = "manager".equals(username) && "manager".equals(password);
        if (isManager) {
            return "manager";
        } else {
            boolean isEmployee = "emp".equals(username) && "emp".equals(password);
            return isEmployee ? "employee" : "error";
        }
    }
}
